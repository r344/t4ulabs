import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testflutter/ui/home/widgets/single_poste_item.dart';
import '../../../core/colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key,required this.user}) : super(key: key);

  final User user;
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(
              top: 25.0,
              bottom: 25.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: ListTile(
                    leading: Container(
                      width: 60.0,
                      height: 60.0,
                      decoration: BoxDecoration(
                        color: const Color(0xff7c94b6),
                        image: const DecorationImage(
                          image: NetworkImage(
                              'https://cdn-icons-png.flaticon.com/512/147/147140.png'),
                          fit: BoxFit.cover,
                        ),
                        borderRadius:
                            const BorderRadius.all(Radius.circular(50.0)),
                        border: Border.all(
                          color: colorsvert,
                          width: 4.0,
                        ),
                      ),
                    ),
                    title: Text(
                      //"Daniela Fernández Ramos",
                      widget.user.email!,
                      style: GoogleFonts.roboto(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: colorsblue,
                      ),
                    ),
                    subtitle: Text(
                      "Lorem ipsum dolor sit amet",
                      style: GoogleFonts.roboto(
                        fontSize: 11,
                        color: Colors.grey,
                      ),
                    ),
                    trailing: InkWell(
                      onTap: () {},
                      child: SvgPicture.asset(
                        "assets/icons/search.svg",
                        width: 20,
                        height: 20,
                      ),
                    ),
                  ),
                ),

                //Future builder
                //notifier list
                SinglePosteItem(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
