import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:testflutter/core/colors.dart';
import 'package:testflutter/ui/home/pages/home_page.dart';
import 'package:testflutter/ui/home/pages/update_video.dart';

class NavigationPage extends StatefulWidget {
  const NavigationPage({Key? key}) : super(key: key);

  @override
  State<NavigationPage> createState() => _NavigationPageState();
}

class _NavigationPageState extends State<NavigationPage> {
  int pageIndex = 0;
  User? user = FirebaseAuth.instance.currentUser!;

  @override
  Widget build(BuildContext context) {
    final pages = [
      HomePage(user: user!),
      const UpdateVideo(),
    ];

    return Scaffold(
      backgroundColor: Colors.white,
      body: pages[pageIndex],
      bottomNavigationBar: buildMyNavBar(context),
    );
  }

  Container buildMyNavBar(BuildContext context) {
    return Container(
      height: 90,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          InkWell(
            onTap: () {
              setState(() {
                pageIndex = 0;
              });
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                pageIndex == 0
                    ? Container(
                        height: 5,
                        width: 65,
                        decoration: const BoxDecoration(
                          color: colorsvert,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                              bottomRight: Radius.circular(5)),
                        ),
                      )
                    : const SizedBox(
                        height: 5,
                        width: 65,
                      ),
                const SizedBox(
                  height: 15,
                ),
                SvgPicture.asset(
                  "assets/icons/homeblackcolor.svg",
                  width: 30,
                  height: 30,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                  width: 65,
                ),
                SvgPicture.asset(
                  "assets/icons/block.svg",
                  width: 30,
                  height: 30,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {
              setState(() {
                pageIndex = 1;
              });
            },
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                pageIndex == 1
                    ? Container(
                        height: 5,
                        width: 65,
                        decoration: const BoxDecoration(
                          color: colorsvert,
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(5),
                              bottomRight: Radius.circular(5)),
                        ),
                      )
                    : const SizedBox(
                        height: 5,
                        width: 65,
                      ),
                SvgPicture.asset(
                  "assets/icons/plus.svg",
                  width: 80,
                  height: 80,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                  width: 65,
                ),
                SvgPicture.asset(
                  "assets/icons/block.svg",
                  width: 30,
                  height: 30,
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () {},
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 20,
                  width: 65,
                ),
                SvgPicture.asset(
                  "assets/icons/block.svg",
                  width: 30,
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
