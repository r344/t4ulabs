import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:testflutter/ui/authentification/authentification.dart';
import 'package:testflutter/ui/home/pages/navigation_page.dart';

class InitialiserPage extends StatelessWidget {
  const InitialiserPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (snapshot.hasError) {
          return const Center(
            child: Text("Server Error"),
          );
        } else if (snapshot.hasData) {
          return const NavigationPage();
        } else {
          return const Authentification();
        }
      });
}
