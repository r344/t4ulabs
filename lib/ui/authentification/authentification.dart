import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:testflutter/core/colors.dart';
import 'package:testflutter/ui/authentification/login.dart';
import 'package:testflutter/ui/authentification/signup.dart';

class Authentification extends StatelessWidget {
  const Authentification({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        tabBarTheme: TabBarTheme(
          labelColor: Colors.white,
          labelStyle: GoogleFonts.roboto(
            fontSize: 24,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ), // color for text
          indicator: const UnderlineTabIndicator(
            borderSide: BorderSide(color: Colors.white),
          ),
        ),
      ),
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            toolbarHeight: 15,
            backgroundColor: colorsvert,
            bottom: const TabBar(
              tabs: [
                Tab(text: "Log In"),
                Tab(text: "Sign Up"),
              ],
            ),
          ),
          body: const TabBarView(
            children: [
              LoginPage(),
              SignUp(),
            ],
          ),
        ),
      ),
    );
  }
}
