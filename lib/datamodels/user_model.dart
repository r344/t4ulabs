// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

class UserModel {
  UserModel({
   required this.id,
   required this.firstName,
   required this.name,
   required this.email,
   required this.password,
   required this.photo,
   required this.aggred,
  });

  final String id;
  final bool firstName;
  final String name;
  final String email;
  final String password;
  final String photo;
  final bool aggred;

  factory UserModel.fromRawJson(String str) => UserModel.fromJson(json.decode(str));

  String toRawJson() => json.encode(toJson());

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
    id: json["id"],
    firstName: json["FirstName"],
    name: json["Name"],
    email: json["Email"],
    password: json["Password"],
    photo: json["Photo"],
    aggred: json["aggred"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "FirstName": firstName,
    "Name": name,
    "Email": email,
    "Password": password,
    "Photo": photo,
    "aggred": aggred,
  };
}
