import 'package:flutter/material.dart';

const Color colorsvert = Color(0xFF195860);
const Color colorsvert2 = Color(0xFF134147);
const Color colorsvert3 = Color(0xFF256168);
const Color colorsblue = Color(0xFF19295C);
const Color colorsbeige = Color(0xFFBFAB88);
const Color colorsgrey = Color(0xFFB8BAC3);
const Color colorsgrey1 = Color(0xFFF2F4F7);