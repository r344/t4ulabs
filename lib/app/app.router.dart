import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';
import 'package:testflutter/ui/authentification/authentification.dart';
import 'package:testflutter/ui/authentification/login.dart';
import 'package:testflutter/ui/intialiser_page.dart';

class Routes {
  static const String initView = '/';

  static const all = <String>{
    initView,
  };
}

class StackedRouter extends RouterBase {
  @override
  List<RouteDef> get routes => _routes;
  final _routes = <RouteDef>[
    RouteDef(Routes.initView, page: InitialiserPage),
  ];

  @override
  // TODO: implement pagesMap
  Map<Type, StackedRouteFactory> get pagesMap => _pagesMap;
  final _pagesMap = <Type, StackedRouteFactory>{
    InitialiserPage: (data) {
      var args = data.getArgs<InitArguments>(
        orElse: () => InitArguments(),
      );
      return MaterialPageRoute<dynamic>(
        builder: (context) => InitialiserPage(
          key: args.key,
        ),
        settings: data,
      );
    }
  };
}

class InitArguments {
  final Key? key;

  InitArguments({this.key});
}
